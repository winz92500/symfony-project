<?php
namespace Blogger\BlogBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BlogAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Article Title'))
            ->add('author', 'text', array('label' => 'Article User'))
            ->add('image', 'text', array('label' => 'Article Image'))
            ->add('tags', 'text', array('label' => 'Article Tags'))
            ->add('blog', 'textarea',  array('attr' => array('class' => 'tinymce'))) 
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('author')
            ->add('image')
            ->add('tags')
            ->add('blog')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('author')
            ->add('image')
            ->add('tags')
            ->add('blog')
        ;
    }
}