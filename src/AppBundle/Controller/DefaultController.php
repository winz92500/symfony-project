<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
    /**
    *@Route("/current-time")
	*/
    public function currentTimeAction()
    {
        return $this->render('exo/index.html.twig',['time' => date('d-m-Y')]);
    }
}
